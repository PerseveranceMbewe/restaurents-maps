const createError = require("http-errors");
const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const indexRouter = require("./routes/index");
const usersRouter = require("./routes/users");
const cors = require("cors");
const app = express();
const { checkdbConnection} =  require("./models/restaurentsModel");

// allow cross origin
app.use(cors());

const restaurentsRouter = require("./routes/restaurents");

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "jade");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));



checkdbConnection((connection, error) => {
  if (error) {
    //something is wrong with the connection
    console.error.bind(connection.message);
  } else {
    console.log("data bae connected successfully!!");
  }
});

app.use("/restaurents/", restaurentsRouter);

module.exports = app;
