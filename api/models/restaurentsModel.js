const mongoose = require("mongoose");
const uri =
  "mongodb+srv://test:test@cluster0.c37s8.mongodb.net/restaurent?retryWrites=true&w=majority";
mongoose.connect(uri, { useNewUrlParser: true });
const db = mongoose.connection;
const Schema = mongoose.Schema;

/**
 * check database  connection
 */
const connectToDB = () => {
  return (callback) => {
    db.on("error", () => {
      return callback({ message: "Error connection to the database" });
    });
    db.once("open", () => {
      // we're connected!
      return callback(
        { message: "Connected to the database successfuly !!" },
        null
      );
    });
  };
};

const restaurentSchema = new mongoose.Schema({
  name: { type: Schema.Types.String },
  lat: { type: Schema.Types.Number },
  long: { type: Schema.Types.Number },
});

exports.checkdbConnection = connectToDB();
exports.restaurentModel = mongoose.model(
  "restaurentModel",
  restaurentSchema,
  "restaurent"
);
