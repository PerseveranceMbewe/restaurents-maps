var express = require("express");
var router = express.Router();
const { restaurentModel } = require("../models/restaurentsModel");

router.get("/", (req, res) => {
  restaurentModel.find({}, (err, data) => {
    if (err) {
      console.log(err);
    } else {
      console.log("results", data);
      res.json(data);
    }
  });
});

module.exports = router;
