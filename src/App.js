import React, { memo, useState, useEffect } from "react";
import axios from "axios";
import GoogleMapReact from "google-map-react";
import Marker from "./components/Markers";
import "./App.css";

const App = () => {
  const [data, setData] = useState([]);
  const [center, setCenter] = useState({ lat: 40.714, lng: -74.005 });
  const [zoom, setZoom] = useState(10);
  const [showMap, setShowMap] = useState(false);
  useEffect(async () => {
    const result = await axios("http://localhost:9000/restaurents/");
    if (result.data) {
      setShowMap(true);
      setData(result.data);
    } else {
      setShowMap(false);
    }
  }, []);

  return (
    <div>
      {(showMap && (
        <div style={{ height: "100vh", width: "100%" }}>
          <GoogleMapReact
            bootstrapURLKeys={{
              key: "AIzaSyDr31XWHwBgl2CKY8ijEfR_fOuLgGuxxMo",
            }}
            defaultCenter={center}
            defaultZoom={zoom}
          >
            {data.map((marker) => (
               <Marker
                lat={marker.lat}
                lng={marker.long}
                name={marker.name}
                
              />
            ))}
          </GoogleMapReact>
        </div>
      )) || (
        <h1 style={{ textAlign: "center" }}>
          Please Start Server to See Restaurents & Refresh Page 
        </h1>
      )}
    </div>
  );
};

export default memo(App);
